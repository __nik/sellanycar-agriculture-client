/* tslint:disable */
//  This file was automatically generated and should not be edited.
import { Injectable } from "@angular/core";
import API, { graphqlOperation } from "@aws-amplify/api";
import { GraphQLResult } from "@aws-amplify/api/lib/types";
import * as Observable from "zen-observable";

export type CreateFieldInput = {
  id?: string | null;
  name?: string | null;
  crop?: Crop | null;
  area?: number | null;
  groupsCanAccess: Array<string | null>;
};

export enum Crop {
  Wheat = "Wheat",
  Broccoli = "Broccoli",
  Strawberry = "Strawberry"
}

export type ModelFieldConditionInput = {
  name?: ModelStringInput | null;
  crop?: ModelCropInput | null;
  area?: ModelFloatInput | null;
  groupsCanAccess?: ModelStringInput | null;
  and?: Array<ModelFieldConditionInput | null> | null;
  or?: Array<ModelFieldConditionInput | null> | null;
  not?: ModelFieldConditionInput | null;
};

export type ModelStringInput = {
  ne?: string | null;
  eq?: string | null;
  le?: string | null;
  lt?: string | null;
  ge?: string | null;
  gt?: string | null;
  contains?: string | null;
  notContains?: string | null;
  between?: Array<string | null> | null;
  beginsWith?: string | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
  size?: ModelSizeInput | null;
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null"
}

export type ModelSizeInput = {
  ne?: number | null;
  eq?: number | null;
  le?: number | null;
  lt?: number | null;
  ge?: number | null;
  gt?: number | null;
  between?: Array<number | null> | null;
};

export type ModelCropInput = {
  eq?: Crop | null;
  ne?: Crop | null;
};

export type ModelFloatInput = {
  ne?: number | null;
  eq?: number | null;
  le?: number | null;
  lt?: number | null;
  ge?: number | null;
  gt?: number | null;
  between?: Array<number | null> | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
};

export enum Status {
  pending_approval = "pending_approval",
  rejected = "rejected",
  ready_to_process = "ready_to_process",
  pending_signoff = "pending_signoff",
  processed = "processed"
}

export type UpdateFieldInput = {
  id: string;
  name?: string | null;
  crop?: Crop | null;
  area?: number | null;
  groupsCanAccess?: Array<string | null> | null;
};

export type DeleteFieldInput = {
  id?: string | null;
};

export type CreateTractorInput = {
  id?: string | null;
  name?: string | null;
  groupsCanAccess: Array<string | null>;
};

export type ModelTractorConditionInput = {
  name?: ModelStringInput | null;
  groupsCanAccess?: ModelStringInput | null;
  and?: Array<ModelTractorConditionInput | null> | null;
  or?: Array<ModelTractorConditionInput | null> | null;
  not?: ModelTractorConditionInput | null;
};

export type UpdateTractorInput = {
  id: string;
  name?: string | null;
  groupsCanAccess?: Array<string | null> | null;
};

export type DeleteTractorInput = {
  id?: string | null;
};

export type CreateProcessedFieldInput = {
  id?: string | null;
  timestamp?: number | null;
  datestring?: string | null;
  area?: number | null;
  status?: Status | null;
  groupsCanAccess: Array<string | null>;
  processedFieldTractorId?: string | null;
  processedFieldFieldId?: string | null;
};

export type ModelProcessedFieldConditionInput = {
  timestamp?: ModelIntInput | null;
  datestring?: ModelStringInput | null;
  area?: ModelFloatInput | null;
  status?: ModelStatusInput | null;
  groupsCanAccess?: ModelStringInput | null;
  and?: Array<ModelProcessedFieldConditionInput | null> | null;
  or?: Array<ModelProcessedFieldConditionInput | null> | null;
  not?: ModelProcessedFieldConditionInput | null;
};

export type ModelIntInput = {
  ne?: number | null;
  eq?: number | null;
  le?: number | null;
  lt?: number | null;
  ge?: number | null;
  gt?: number | null;
  between?: Array<number | null> | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
};

export type ModelStatusInput = {
  eq?: Status | null;
  ne?: Status | null;
};

export type UpdateProcessedFieldInput = {
  id: string;
  timestamp?: number | null;
  datestring?: string | null;
  area?: number | null;
  status?: Status | null;
  groupsCanAccess?: Array<string | null> | null;
  processedFieldTractorId?: string | null;
  processedFieldFieldId?: string | null;
};

export type DeleteProcessedFieldInput = {
  id?: string | null;
};

export type ModelFieldFilterInput = {
  id?: ModelIDInput | null;
  name?: ModelStringInput | null;
  crop?: ModelCropInput | null;
  area?: ModelFloatInput | null;
  groupsCanAccess?: ModelStringInput | null;
  and?: Array<ModelFieldFilterInput | null> | null;
  or?: Array<ModelFieldFilterInput | null> | null;
  not?: ModelFieldFilterInput | null;
};

export type ModelIDInput = {
  ne?: string | null;
  eq?: string | null;
  le?: string | null;
  lt?: string | null;
  ge?: string | null;
  gt?: string | null;
  contains?: string | null;
  notContains?: string | null;
  between?: Array<string | null> | null;
  beginsWith?: string | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
  size?: ModelSizeInput | null;
};

export type ModelTractorFilterInput = {
  id?: ModelIDInput | null;
  name?: ModelStringInput | null;
  groupsCanAccess?: ModelStringInput | null;
  and?: Array<ModelTractorFilterInput | null> | null;
  or?: Array<ModelTractorFilterInput | null> | null;
  not?: ModelTractorFilterInput | null;
};

export type ModelProcessedFieldFilterInput = {
  id?: ModelIDInput | null;
  timestamp?: ModelIntInput | null;
  datestring?: ModelStringInput | null;
  area?: ModelFloatInput | null;
  status?: ModelStatusInput | null;
  groupsCanAccess?: ModelStringInput | null;
  and?: Array<ModelProcessedFieldFilterInput | null> | null;
  or?: Array<ModelProcessedFieldFilterInput | null> | null;
  not?: ModelProcessedFieldFilterInput | null;
};

export type CreateFieldMutation = {
  __typename: "Field";
  id: string;
  name: string | null;
  crop: Crop | null;
  area: number | null;
  groupsCanAccess: Array<string | null>;
  ProcessedField: {
    __typename: "ModelProcessedFieldConnection";
    items: Array<{
      __typename: "ProcessedField";
      id: string;
      timestamp: number | null;
      datestring: string | null;
      area: number | null;
      status: Status | null;
      groupsCanAccess: Array<string | null>;
      owner: string | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
  owner: string | null;
};

export type UpdateFieldMutation = {
  __typename: "Field";
  id: string;
  name: string | null;
  crop: Crop | null;
  area: number | null;
  groupsCanAccess: Array<string | null>;
  ProcessedField: {
    __typename: "ModelProcessedFieldConnection";
    items: Array<{
      __typename: "ProcessedField";
      id: string;
      timestamp: number | null;
      datestring: string | null;
      area: number | null;
      status: Status | null;
      groupsCanAccess: Array<string | null>;
      owner: string | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
  owner: string | null;
};

export type DeleteFieldMutation = {
  __typename: "Field";
  id: string;
  name: string | null;
  crop: Crop | null;
  area: number | null;
  groupsCanAccess: Array<string | null>;
  ProcessedField: {
    __typename: "ModelProcessedFieldConnection";
    items: Array<{
      __typename: "ProcessedField";
      id: string;
      timestamp: number | null;
      datestring: string | null;
      area: number | null;
      status: Status | null;
      groupsCanAccess: Array<string | null>;
      owner: string | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
  owner: string | null;
};

export type CreateTractorMutation = {
  __typename: "Tractor";
  id: string;
  name: string | null;
  groupsCanAccess: Array<string | null>;
  ProcessedField: {
    __typename: "ModelProcessedFieldConnection";
    items: Array<{
      __typename: "ProcessedField";
      id: string;
      timestamp: number | null;
      datestring: string | null;
      area: number | null;
      status: Status | null;
      groupsCanAccess: Array<string | null>;
      owner: string | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
  owner: string | null;
};

export type UpdateTractorMutation = {
  __typename: "Tractor";
  id: string;
  name: string | null;
  groupsCanAccess: Array<string | null>;
  ProcessedField: {
    __typename: "ModelProcessedFieldConnection";
    items: Array<{
      __typename: "ProcessedField";
      id: string;
      timestamp: number | null;
      datestring: string | null;
      area: number | null;
      status: Status | null;
      groupsCanAccess: Array<string | null>;
      owner: string | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
  owner: string | null;
};

export type DeleteTractorMutation = {
  __typename: "Tractor";
  id: string;
  name: string | null;
  groupsCanAccess: Array<string | null>;
  ProcessedField: {
    __typename: "ModelProcessedFieldConnection";
    items: Array<{
      __typename: "ProcessedField";
      id: string;
      timestamp: number | null;
      datestring: string | null;
      area: number | null;
      status: Status | null;
      groupsCanAccess: Array<string | null>;
      owner: string | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
  owner: string | null;
};

export type CreateProcessedFieldMutation = {
  __typename: "ProcessedField";
  id: string;
  timestamp: number | null;
  datestring: string | null;
  area: number | null;
  tractor: {
    __typename: "Tractor";
    id: string;
    name: string | null;
    groupsCanAccess: Array<string | null>;
    ProcessedField: {
      __typename: "ModelProcessedFieldConnection";
      nextToken: string | null;
    } | null;
    owner: string | null;
  } | null;
  field: {
    __typename: "Field";
    id: string;
    name: string | null;
    crop: Crop | null;
    area: number | null;
    groupsCanAccess: Array<string | null>;
    ProcessedField: {
      __typename: "ModelProcessedFieldConnection";
      nextToken: string | null;
    } | null;
    owner: string | null;
  } | null;
  status: Status | null;
  groupsCanAccess: Array<string | null>;
  owner: string | null;
};

export type UpdateProcessedFieldMutation = {
  __typename: "ProcessedField";
  id: string;
  timestamp: number | null;
  datestring: string | null;
  area: number | null;
  tractor: {
    __typename: "Tractor";
    id: string;
    name: string | null;
    groupsCanAccess: Array<string | null>;
    ProcessedField: {
      __typename: "ModelProcessedFieldConnection";
      nextToken: string | null;
    } | null;
    owner: string | null;
  } | null;
  field: {
    __typename: "Field";
    id: string;
    name: string | null;
    crop: Crop | null;
    area: number | null;
    groupsCanAccess: Array<string | null>;
    ProcessedField: {
      __typename: "ModelProcessedFieldConnection";
      nextToken: string | null;
    } | null;
    owner: string | null;
  } | null;
  status: Status | null;
  groupsCanAccess: Array<string | null>;
  owner: string | null;
};

export type DeleteProcessedFieldMutation = {
  __typename: "ProcessedField";
  id: string;
  timestamp: number | null;
  datestring: string | null;
  area: number | null;
  tractor: {
    __typename: "Tractor";
    id: string;
    name: string | null;
    groupsCanAccess: Array<string | null>;
    ProcessedField: {
      __typename: "ModelProcessedFieldConnection";
      nextToken: string | null;
    } | null;
    owner: string | null;
  } | null;
  field: {
    __typename: "Field";
    id: string;
    name: string | null;
    crop: Crop | null;
    area: number | null;
    groupsCanAccess: Array<string | null>;
    ProcessedField: {
      __typename: "ModelProcessedFieldConnection";
      nextToken: string | null;
    } | null;
    owner: string | null;
  } | null;
  status: Status | null;
  groupsCanAccess: Array<string | null>;
  owner: string | null;
};

export type GetFieldQuery = {
  __typename: "Field";
  id: string;
  name: string | null;
  crop: Crop | null;
  area: number | null;
  groupsCanAccess: Array<string | null>;
  ProcessedField: {
    __typename: "ModelProcessedFieldConnection";
    items: Array<{
      __typename: "ProcessedField";
      id: string;
      timestamp: number | null;
      datestring: string | null;
      area: number | null;
      status: Status | null;
      groupsCanAccess: Array<string | null>;
      owner: string | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
  owner: string | null;
};

export type ListFieldsQuery = {
  __typename: "ModelFieldConnection";
  items: Array<{
    __typename: "Field";
    id: string;
    name: string | null;
    crop: Crop | null;
    area: number | null;
    groupsCanAccess: Array<string | null>;
    ProcessedField: {
      __typename: "ModelProcessedFieldConnection";
      nextToken: string | null;
    } | null;
    owner: string | null;
  } | null> | null;
  nextToken: string | null;
};

export type GetTractorQuery = {
  __typename: "Tractor";
  id: string;
  name: string | null;
  groupsCanAccess: Array<string | null>;
  ProcessedField: {
    __typename: "ModelProcessedFieldConnection";
    items: Array<{
      __typename: "ProcessedField";
      id: string;
      timestamp: number | null;
      datestring: string | null;
      area: number | null;
      status: Status | null;
      groupsCanAccess: Array<string | null>;
      owner: string | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
  owner: string | null;
};

export type ListTractorsQuery = {
  __typename: "ModelTractorConnection";
  items: Array<{
    __typename: "Tractor";
    id: string;
    name: string | null;
    groupsCanAccess: Array<string | null>;
    ProcessedField: {
      __typename: "ModelProcessedFieldConnection";
      nextToken: string | null;
    } | null;
    owner: string | null;
  } | null> | null;
  nextToken: string | null;
};

export type GetProcessedFieldQuery = {
  __typename: "ProcessedField";
  id: string;
  timestamp: number | null;
  datestring: string | null;
  area: number | null;
  tractor: {
    __typename: "Tractor";
    id: string;
    name: string | null;
    groupsCanAccess: Array<string | null>;
    ProcessedField: {
      __typename: "ModelProcessedFieldConnection";
      nextToken: string | null;
    } | null;
    owner: string | null;
  } | null;
  field: {
    __typename: "Field";
    id: string;
    name: string | null;
    crop: Crop | null;
    area: number | null;
    groupsCanAccess: Array<string | null>;
    ProcessedField: {
      __typename: "ModelProcessedFieldConnection";
      nextToken: string | null;
    } | null;
    owner: string | null;
  } | null;
  status: Status | null;
  groupsCanAccess: Array<string | null>;
  owner: string | null;
};

export type ListProcessedFieldsQuery = {
  __typename: "ModelProcessedFieldConnection";
  items: Array<{
    __typename: "ProcessedField";
    id: string;
    timestamp: number | null;
    datestring: string | null;
    area: number | null;
    tractor: {
      __typename: "Tractor";
      id: string;
      name: string | null;
      groupsCanAccess: Array<string | null>;
      owner: string | null;
    } | null;
    field: {
      __typename: "Field";
      id: string;
      name: string | null;
      crop: Crop | null;
      area: number | null;
      groupsCanAccess: Array<string | null>;
      owner: string | null;
    } | null;
    status: Status | null;
    groupsCanAccess: Array<string | null>;
    owner: string | null;
  } | null> | null;
  nextToken: string | null;
};

export type OnCreateFieldSubscription = {
  __typename: "Field";
  id: string;
  name: string | null;
  crop: Crop | null;
  area: number | null;
  groupsCanAccess: Array<string | null>;
  ProcessedField: {
    __typename: "ModelProcessedFieldConnection";
    items: Array<{
      __typename: "ProcessedField";
      id: string;
      timestamp: number | null;
      datestring: string | null;
      area: number | null;
      status: Status | null;
      groupsCanAccess: Array<string | null>;
      owner: string | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
  owner: string | null;
};

export type OnUpdateFieldSubscription = {
  __typename: "Field";
  id: string;
  name: string | null;
  crop: Crop | null;
  area: number | null;
  groupsCanAccess: Array<string | null>;
  ProcessedField: {
    __typename: "ModelProcessedFieldConnection";
    items: Array<{
      __typename: "ProcessedField";
      id: string;
      timestamp: number | null;
      datestring: string | null;
      area: number | null;
      status: Status | null;
      groupsCanAccess: Array<string | null>;
      owner: string | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
  owner: string | null;
};

export type OnDeleteFieldSubscription = {
  __typename: "Field";
  id: string;
  name: string | null;
  crop: Crop | null;
  area: number | null;
  groupsCanAccess: Array<string | null>;
  ProcessedField: {
    __typename: "ModelProcessedFieldConnection";
    items: Array<{
      __typename: "ProcessedField";
      id: string;
      timestamp: number | null;
      datestring: string | null;
      area: number | null;
      status: Status | null;
      groupsCanAccess: Array<string | null>;
      owner: string | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
  owner: string | null;
};

export type OnCreateTractorSubscription = {
  __typename: "Tractor";
  id: string;
  name: string | null;
  groupsCanAccess: Array<string | null>;
  ProcessedField: {
    __typename: "ModelProcessedFieldConnection";
    items: Array<{
      __typename: "ProcessedField";
      id: string;
      timestamp: number | null;
      datestring: string | null;
      area: number | null;
      status: Status | null;
      groupsCanAccess: Array<string | null>;
      owner: string | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
  owner: string | null;
};

export type OnUpdateTractorSubscription = {
  __typename: "Tractor";
  id: string;
  name: string | null;
  groupsCanAccess: Array<string | null>;
  ProcessedField: {
    __typename: "ModelProcessedFieldConnection";
    items: Array<{
      __typename: "ProcessedField";
      id: string;
      timestamp: number | null;
      datestring: string | null;
      area: number | null;
      status: Status | null;
      groupsCanAccess: Array<string | null>;
      owner: string | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
  owner: string | null;
};

export type OnDeleteTractorSubscription = {
  __typename: "Tractor";
  id: string;
  name: string | null;
  groupsCanAccess: Array<string | null>;
  ProcessedField: {
    __typename: "ModelProcessedFieldConnection";
    items: Array<{
      __typename: "ProcessedField";
      id: string;
      timestamp: number | null;
      datestring: string | null;
      area: number | null;
      status: Status | null;
      groupsCanAccess: Array<string | null>;
      owner: string | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
  owner: string | null;
};

export type OnCreateProcessedFieldSubscription = {
  __typename: "ProcessedField";
  id: string;
  timestamp: number | null;
  datestring: string | null;
  area: number | null;
  tractor: {
    __typename: "Tractor";
    id: string;
    name: string | null;
    groupsCanAccess: Array<string | null>;
    ProcessedField: {
      __typename: "ModelProcessedFieldConnection";
      nextToken: string | null;
    } | null;
    owner: string | null;
  } | null;
  field: {
    __typename: "Field";
    id: string;
    name: string | null;
    crop: Crop | null;
    area: number | null;
    groupsCanAccess: Array<string | null>;
    ProcessedField: {
      __typename: "ModelProcessedFieldConnection";
      nextToken: string | null;
    } | null;
    owner: string | null;
  } | null;
  status: Status | null;
  groupsCanAccess: Array<string | null>;
  owner: string | null;
};

export type OnUpdateProcessedFieldSubscription = {
  __typename: "ProcessedField";
  id: string;
  timestamp: number | null;
  datestring: string | null;
  area: number | null;
  tractor: {
    __typename: "Tractor";
    id: string;
    name: string | null;
    groupsCanAccess: Array<string | null>;
    ProcessedField: {
      __typename: "ModelProcessedFieldConnection";
      nextToken: string | null;
    } | null;
    owner: string | null;
  } | null;
  field: {
    __typename: "Field";
    id: string;
    name: string | null;
    crop: Crop | null;
    area: number | null;
    groupsCanAccess: Array<string | null>;
    ProcessedField: {
      __typename: "ModelProcessedFieldConnection";
      nextToken: string | null;
    } | null;
    owner: string | null;
  } | null;
  status: Status | null;
  groupsCanAccess: Array<string | null>;
  owner: string | null;
};

export type OnDeleteProcessedFieldSubscription = {
  __typename: "ProcessedField";
  id: string;
  timestamp: number | null;
  datestring: string | null;
  area: number | null;
  tractor: {
    __typename: "Tractor";
    id: string;
    name: string | null;
    groupsCanAccess: Array<string | null>;
    ProcessedField: {
      __typename: "ModelProcessedFieldConnection";
      nextToken: string | null;
    } | null;
    owner: string | null;
  } | null;
  field: {
    __typename: "Field";
    id: string;
    name: string | null;
    crop: Crop | null;
    area: number | null;
    groupsCanAccess: Array<string | null>;
    ProcessedField: {
      __typename: "ModelProcessedFieldConnection";
      nextToken: string | null;
    } | null;
    owner: string | null;
  } | null;
  status: Status | null;
  groupsCanAccess: Array<string | null>;
  owner: string | null;
};

@Injectable({
  providedIn: "root"
})
export class APIService {
  async CreateField(
    input: CreateFieldInput,
    condition?: ModelFieldConditionInput
  ): Promise<CreateFieldMutation> {
    const statement = `mutation CreateField($input: CreateFieldInput!, $condition: ModelFieldConditionInput) {
        createField(input: $input, condition: $condition) {
          __typename
          id
          name
          crop
          area
          groupsCanAccess
          ProcessedField {
            __typename
            items {
              __typename
              id
              timestamp
              datestring
              area
              status
              groupsCanAccess
              owner
            }
            nextToken
          }
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateFieldMutation>response.data.createField;
  }
  async UpdateField(
    input: UpdateFieldInput,
    condition?: ModelFieldConditionInput
  ): Promise<UpdateFieldMutation> {
    const statement = `mutation UpdateField($input: UpdateFieldInput!, $condition: ModelFieldConditionInput) {
        updateField(input: $input, condition: $condition) {
          __typename
          id
          name
          crop
          area
          groupsCanAccess
          ProcessedField {
            __typename
            items {
              __typename
              id
              timestamp
              datestring
              area
              status
              groupsCanAccess
              owner
            }
            nextToken
          }
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateFieldMutation>response.data.updateField;
  }
  async DeleteField(
    input: DeleteFieldInput,
    condition?: ModelFieldConditionInput
  ): Promise<DeleteFieldMutation> {
    const statement = `mutation DeleteField($input: DeleteFieldInput!, $condition: ModelFieldConditionInput) {
        deleteField(input: $input, condition: $condition) {
          __typename
          id
          name
          crop
          area
          groupsCanAccess
          ProcessedField {
            __typename
            items {
              __typename
              id
              timestamp
              datestring
              area
              status
              groupsCanAccess
              owner
            }
            nextToken
          }
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteFieldMutation>response.data.deleteField;
  }
  async CreateTractor(
    input: CreateTractorInput,
    condition?: ModelTractorConditionInput
  ): Promise<CreateTractorMutation> {
    const statement = `mutation CreateTractor($input: CreateTractorInput!, $condition: ModelTractorConditionInput) {
        createTractor(input: $input, condition: $condition) {
          __typename
          id
          name
          groupsCanAccess
          ProcessedField {
            __typename
            items {
              __typename
              id
              timestamp
              datestring
              area
              status
              groupsCanAccess
              owner
            }
            nextToken
          }
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateTractorMutation>response.data.createTractor;
  }
  async UpdateTractor(
    input: UpdateTractorInput,
    condition?: ModelTractorConditionInput
  ): Promise<UpdateTractorMutation> {
    const statement = `mutation UpdateTractor($input: UpdateTractorInput!, $condition: ModelTractorConditionInput) {
        updateTractor(input: $input, condition: $condition) {
          __typename
          id
          name
          groupsCanAccess
          ProcessedField {
            __typename
            items {
              __typename
              id
              timestamp
              datestring
              area
              status
              groupsCanAccess
              owner
            }
            nextToken
          }
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateTractorMutation>response.data.updateTractor;
  }
  async DeleteTractor(
    input: DeleteTractorInput,
    condition?: ModelTractorConditionInput
  ): Promise<DeleteTractorMutation> {
    const statement = `mutation DeleteTractor($input: DeleteTractorInput!, $condition: ModelTractorConditionInput) {
        deleteTractor(input: $input, condition: $condition) {
          __typename
          id
          name
          groupsCanAccess
          ProcessedField {
            __typename
            items {
              __typename
              id
              timestamp
              datestring
              area
              status
              groupsCanAccess
              owner
            }
            nextToken
          }
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteTractorMutation>response.data.deleteTractor;
  }
  async CreateProcessedField(
    input: CreateProcessedFieldInput,
    condition?: ModelProcessedFieldConditionInput
  ): Promise<CreateProcessedFieldMutation> {
    const statement = `mutation CreateProcessedField($input: CreateProcessedFieldInput!, $condition: ModelProcessedFieldConditionInput) {
        createProcessedField(input: $input, condition: $condition) {
          __typename
          id
          timestamp
          datestring
          area
          tractor {
            __typename
            id
            name
            groupsCanAccess
            ProcessedField {
              __typename
              nextToken
            }
            owner
          }
          field {
            __typename
            id
            name
            crop
            area
            groupsCanAccess
            ProcessedField {
              __typename
              nextToken
            }
            owner
          }
          status
          groupsCanAccess
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateProcessedFieldMutation>response.data.createProcessedField;
  }
  async UpdateProcessedField(
    input: UpdateProcessedFieldInput,
    condition?: ModelProcessedFieldConditionInput
  ): Promise<UpdateProcessedFieldMutation> {
    const statement = `mutation UpdateProcessedField($input: UpdateProcessedFieldInput!, $condition: ModelProcessedFieldConditionInput) {
        updateProcessedField(input: $input, condition: $condition) {
          __typename
          id
          timestamp
          datestring
          area
          tractor {
            __typename
            id
            name
            groupsCanAccess
            ProcessedField {
              __typename
              nextToken
            }
            owner
          }
          field {
            __typename
            id
            name
            crop
            area
            groupsCanAccess
            ProcessedField {
              __typename
              nextToken
            }
            owner
          }
          status
          groupsCanAccess
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateProcessedFieldMutation>response.data.updateProcessedField;
  }
  async DeleteProcessedField(
    input: DeleteProcessedFieldInput,
    condition?: ModelProcessedFieldConditionInput
  ): Promise<DeleteProcessedFieldMutation> {
    const statement = `mutation DeleteProcessedField($input: DeleteProcessedFieldInput!, $condition: ModelProcessedFieldConditionInput) {
        deleteProcessedField(input: $input, condition: $condition) {
          __typename
          id
          timestamp
          datestring
          area
          tractor {
            __typename
            id
            name
            groupsCanAccess
            ProcessedField {
              __typename
              nextToken
            }
            owner
          }
          field {
            __typename
            id
            name
            crop
            area
            groupsCanAccess
            ProcessedField {
              __typename
              nextToken
            }
            owner
          }
          status
          groupsCanAccess
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteProcessedFieldMutation>response.data.deleteProcessedField;
  }
  async GetField(id: string): Promise<GetFieldQuery> {
    const statement = `query GetField($id: ID!) {
        getField(id: $id) {
          __typename
          id
          name
          crop
          area
          groupsCanAccess
          ProcessedField {
            __typename
            items {
              __typename
              id
              timestamp
              datestring
              area
              status
              groupsCanAccess
              owner
            }
            nextToken
          }
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetFieldQuery>response.data.getField;
  }
  async ListFields(
    filter?: ModelFieldFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListFieldsQuery> {
    const statement = `query ListFields($filter: ModelFieldFilterInput, $limit: Int, $nextToken: String) {
        listFields(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            name
            crop
            area
            groupsCanAccess
            ProcessedField {
              __typename
              nextToken
            }
            owner
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListFieldsQuery>response.data.listFields;
  }
  async GetTractor(id: string): Promise<GetTractorQuery> {
    const statement = `query GetTractor($id: ID!) {
        getTractor(id: $id) {
          __typename
          id
          name
          groupsCanAccess
          ProcessedField {
            __typename
            items {
              __typename
              id
              timestamp
              datestring
              area
              status
              groupsCanAccess
              owner
            }
            nextToken
          }
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetTractorQuery>response.data.getTractor;
  }
  async ListTractors(
    filter?: ModelTractorFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListTractorsQuery> {
    const statement = `query ListTractors($filter: ModelTractorFilterInput, $limit: Int, $nextToken: String) {
        listTractors(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            name
            groupsCanAccess
            ProcessedField {
              __typename
              nextToken
            }
            owner
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListTractorsQuery>response.data.listTractors;
  }
  async GetProcessedField(id: string): Promise<GetProcessedFieldQuery> {
    const statement = `query GetProcessedField($id: ID!) {
        getProcessedField(id: $id) {
          __typename
          id
          timestamp
          datestring
          area
          tractor {
            __typename
            id
            name
            groupsCanAccess
            ProcessedField {
              __typename
              nextToken
            }
            owner
          }
          field {
            __typename
            id
            name
            crop
            area
            groupsCanAccess
            ProcessedField {
              __typename
              nextToken
            }
            owner
          }
          status
          groupsCanAccess
          owner
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetProcessedFieldQuery>response.data.getProcessedField;
  }
  async ListProcessedFields(
    filter?: ModelProcessedFieldFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListProcessedFieldsQuery> {
    const statement = `query ListProcessedFields($filter: ModelProcessedFieldFilterInput, $limit: Int, $nextToken: String) {
        listProcessedFields(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            timestamp
            datestring
            area
            tractor {
              __typename
              id
              name
              groupsCanAccess
              owner
            }
            field {
              __typename
              id
              name
              crop
              area
              groupsCanAccess
              owner
            }
            status
            groupsCanAccess
            owner
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListProcessedFieldsQuery>response.data.listProcessedFields;
  }
  OnCreateFieldListener: Observable<OnCreateFieldSubscription> = API.graphql(
    graphqlOperation(
      `subscription OnCreateField($owner: String) {
        onCreateField(owner: $owner) {
          __typename
          id
          name
          crop
          area
          groupsCanAccess
          ProcessedField {
            __typename
            items {
              __typename
              id
              timestamp
              datestring
              area
              status
              groupsCanAccess
              owner
            }
            nextToken
          }
          owner
        }
      }`
    )
  ) as Observable<OnCreateFieldSubscription>;

  OnUpdateFieldListener: Observable<OnUpdateFieldSubscription> = API.graphql(
    graphqlOperation(
      `subscription OnUpdateField($owner: String) {
        onUpdateField(owner: $owner) {
          __typename
          id
          name
          crop
          area
          groupsCanAccess
          ProcessedField {
            __typename
            items {
              __typename
              id
              timestamp
              datestring
              area
              status
              groupsCanAccess
              owner
            }
            nextToken
          }
          owner
        }
      }`
    )
  ) as Observable<OnUpdateFieldSubscription>;

  OnDeleteFieldListener: Observable<OnDeleteFieldSubscription> = API.graphql(
    graphqlOperation(
      `subscription OnDeleteField($owner: String) {
        onDeleteField(owner: $owner) {
          __typename
          id
          name
          crop
          area
          groupsCanAccess
          ProcessedField {
            __typename
            items {
              __typename
              id
              timestamp
              datestring
              area
              status
              groupsCanAccess
              owner
            }
            nextToken
          }
          owner
        }
      }`
    )
  ) as Observable<OnDeleteFieldSubscription>;

  OnCreateTractorListener: Observable<
    OnCreateTractorSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnCreateTractor($owner: String) {
        onCreateTractor(owner: $owner) {
          __typename
          id
          name
          groupsCanAccess
          ProcessedField {
            __typename
            items {
              __typename
              id
              timestamp
              datestring
              area
              status
              groupsCanAccess
              owner
            }
            nextToken
          }
          owner
        }
      }`
    )
  ) as Observable<OnCreateTractorSubscription>;

  OnUpdateTractorListener: Observable<
    OnUpdateTractorSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnUpdateTractor($owner: String) {
        onUpdateTractor(owner: $owner) {
          __typename
          id
          name
          groupsCanAccess
          ProcessedField {
            __typename
            items {
              __typename
              id
              timestamp
              datestring
              area
              status
              groupsCanAccess
              owner
            }
            nextToken
          }
          owner
        }
      }`
    )
  ) as Observable<OnUpdateTractorSubscription>;

  OnDeleteTractorListener: Observable<
    OnDeleteTractorSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnDeleteTractor($owner: String) {
        onDeleteTractor(owner: $owner) {
          __typename
          id
          name
          groupsCanAccess
          ProcessedField {
            __typename
            items {
              __typename
              id
              timestamp
              datestring
              area
              status
              groupsCanAccess
              owner
            }
            nextToken
          }
          owner
        }
      }`
    )
  ) as Observable<OnDeleteTractorSubscription>;

  OnCreateProcessedFieldListener: Observable<
    OnCreateProcessedFieldSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnCreateProcessedField($owner: String) {
        onCreateProcessedField(owner: $owner) {
          __typename
          id
          timestamp
          datestring
          area
          tractor {
            __typename
            id
            name
            groupsCanAccess
            ProcessedField {
              __typename
              nextToken
            }
            owner
          }
          field {
            __typename
            id
            name
            crop
            area
            groupsCanAccess
            ProcessedField {
              __typename
              nextToken
            }
            owner
          }
          status
          groupsCanAccess
          owner
        }
      }`
    )
  ) as Observable<OnCreateProcessedFieldSubscription>;

  OnUpdateProcessedFieldListener: Observable<
    OnUpdateProcessedFieldSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnUpdateProcessedField($owner: String) {
        onUpdateProcessedField(owner: $owner) {
          __typename
          id
          timestamp
          datestring
          area
          tractor {
            __typename
            id
            name
            groupsCanAccess
            ProcessedField {
              __typename
              nextToken
            }
            owner
          }
          field {
            __typename
            id
            name
            crop
            area
            groupsCanAccess
            ProcessedField {
              __typename
              nextToken
            }
            owner
          }
          status
          groupsCanAccess
          owner
        }
      }`
    )
  ) as Observable<OnUpdateProcessedFieldSubscription>;

  OnDeleteProcessedFieldListener: Observable<
    OnDeleteProcessedFieldSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnDeleteProcessedField($owner: String) {
        onDeleteProcessedField(owner: $owner) {
          __typename
          id
          timestamp
          datestring
          area
          tractor {
            __typename
            id
            name
            groupsCanAccess
            ProcessedField {
              __typename
              nextToken
            }
            owner
          }
          field {
            __typename
            id
            name
            crop
            area
            groupsCanAccess
            ProcessedField {
              __typename
              nextToken
            }
            owner
          }
          status
          groupsCanAccess
          owner
        }
      }`
    )
  ) as Observable<OnDeleteProcessedFieldSubscription>;
}
