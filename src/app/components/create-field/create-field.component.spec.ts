import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateFieldComponent } from './create-field.component';
import { FormsModule } from '@angular/forms';
import Amplify, { API } from 'aws-amplify';
import aws_exports from '../../../aws-exports';
import { By } from '@angular/platform-browser';
import { APIService } from 'src/app/API.service';

describe('CreateFieldComponent', () => {
  let component: CreateFieldComponent;
  let fixture: ComponentFixture<CreateFieldComponent>;
  let Api;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateFieldComponent],
      imports: [
        IonicModule.forRoot(),
        FormsModule
      ],
      providers: [
        APIService
      ]
    }).compileComponents();

    Amplify.configure(aws_exports);
    API.configure(aws_exports);

    fixture = TestBed.createComponent(CreateFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    Api = fixture.debugElement.injector.get(APIService);
  
    spyOn(Api,'CreateField').and.callFake(()=>{ Api.ListFields() });
    spyOn(Api,'UpdateField').and.callFake(()=>{ Api.ListFields() });
    spyOn(Api,'ListFields').and.callFake(()=>{return;});

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call Amplify API to make a field + list all fields', ()=>{
    component.ngOnInit();
    component.area = 1234;
    component.crop = 'Strawberry';
    component.name = 'Strawberry field';
    component.groupsCanAccess = ['users'];
    fixture.detectChanges();
    fixture.debugElement.query(By.css('#create-field-button')).triggerEventHandler('click',null);

    expect(Api.CreateField).toHaveBeenCalled();
    expect(Api.ListFields).toHaveBeenCalled();
    expect(Api.CreateField).toHaveBeenCalledBefore(Api.ListFields);
  });

  it('should update if id is present', ()=>{
    component.ngOnInit();

    component.id = 1234;
    component.area = 1234;
    component.crop = 'Strawberry';
    component.name = 'Strawberry field';
    fixture.detectChanges();
    
    fixture.debugElement.query(By.css('#create-field-button')).triggerEventHandler('click',null);

    expect(Api.UpdateField).toHaveBeenCalled();
    expect(Api.ListFields).toHaveBeenCalled();
  })

});
