import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { APIService } from 'src/app/API.service';
import { ConstantService } from 'src/app/service/constant.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-create-field',
  templateUrl: './create-field.component.html',
  styleUrls: ['./create-field.component.scss'],
})
export class CreateFieldComponent implements OnInit {

  name;
  area;
  crop;
  id = null;
  groupsCanAccess;
  err = null;

  constructor(
    public APIService: APIService,
    public modalController: ModalController,
    public constant: ConstantService
  ) { }

  ngOnInit() {}

  create(){
    console.log(this.name,this.area);
    if(_.isEmpty(this.name)){
      this.err = 'Enter a name for the field';
      return;
    }
    if(this.area < 1){
      this.err = 'Enter an area for the field. How big is the field?';
      return;
    }
    
    if(this.id != null){
      // update
      this.APIService.UpdateField({
        id: this.id,
        name: this.name,
        area: this.area,
        crop: this.crop
      })
      .then(()=>{
        this.APIService.ListFields()
          .then((fields) => {
            _.set(this.constant,'fields',_.orderBy(fields.items,['name'],['asc']));
            this.modalController.dismiss('update');
          })
          .catch((err) => {
            console.log('Err', err);
          })
      })
    }
    else{
      // create
      this.APIService.CreateField({
        name: this.name,
        area: this.area,
        crop: this.crop,
        groupsCanAccess: ['users']
      })
      .then(()=>{
        this.APIService.ListFields()
          .then((fields) => {
            _.set(this.constant,'fields',_.orderBy(fields.items,['name'],['asc']));
            this.modalController.dismiss('create');
          })
          .catch((err) => {
            console.log('Err', err);
          })
      });
    }
  }

}
