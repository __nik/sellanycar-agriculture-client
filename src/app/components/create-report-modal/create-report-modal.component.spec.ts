import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import Amplify, { API } from 'aws-amplify';
import aws_exports from '../../../aws-exports';
import { CreateReportModalComponent } from './create-report-modal.component';
import { FormsModule } from '@angular/forms';
import { APIService } from 'src/app/API.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('Create report modal', () => {
  let component: CreateReportModalComponent;
  let fixture: ComponentFixture<CreateReportModalComponent>;
  let Api;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateReportModalComponent],
      imports: [
        IonicModule.forRoot(),
        FormsModule
      ],
      providers: [
        APIService
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();

    Amplify.configure(aws_exports);
    API.configure(aws_exports);

    fixture = TestBed.createComponent(CreateReportModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    Api = fixture.debugElement.injector.get(APIService);

    spyOn(Api, 'ListProcessedFields').and.callFake(() => { return; });
    spyOn(component, 'checked');

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should validate report creation', () => {
    fixture.debugElement.query(By.css('#create-field-button')).triggerEventHandler('click', null);
    expect(Api.ListProcessedFields).toHaveBeenCalled();
  });
  it('should show total area processed as summary during report creation', () => {
    component.report = [
      { 
        id: "some-generated-id",
        tractor:{
          name: "Some tractor"
        },
        field:{
          name: "Some field",
          crop: "Some crop"
        },
        datestring: "12/3/2020",
        area: 1311,
        status: "processed",
        owner: "nikhil2905"
      }
    ];
    
    component.area_processed = 1231;
    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('#report-block'))).not.toBeNull();
  });
});
