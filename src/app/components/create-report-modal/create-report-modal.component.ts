import { Component, OnInit } from '@angular/core';
import { APIService, Status } from 'src/app/API.service';
import { ConstantService } from 'src/app/service/constant.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-create-report-modal',
  templateUrl: './create-report-modal.component.html',
  styleUrls: ['./create-report-modal.component.scss'],
})
export class CreateReportModalComponent implements OnInit {

  fields;
  tractors;
  err;
  field = [];
  tractor = [];
  crop = [];
  max_date;
  area_processed;
  fields_processed;
  report = [];
  report_err:any = false;
  crops = [
    {
      name: "Strawberry",
      id: "Strawberry"
    },
    {
      name: "Broccoli",
      id: "Broccoli"
    },
    {
      name: "Wheat",
      id: "Wheat"
    }
  ];
  timestamp;
  constructor(
    public APIService: APIService,
    public constant: ConstantService
  ) { }

  ngOnInit() {
    let MyDate = new Date();

    MyDate.setDate(MyDate.getDate());

    this.max_date = MyDate.getFullYear() + '-'
                + ('0' + (MyDate.getMonth()+1)).slice(-2) + '-'
                + ('0' + MyDate.getDate()).slice(-2);
    console.log(this.max_date);
  }

  checked(res,id,ev=null){
    console.log(ev.target.checked);
    if(ev.target.checked){
      // Add
      if(res=='field'){
        this.field.push(id);
        this.field = _.uniq(this.field);
      }
      else if(res=='tractor'){
        this.tractor.push(id);
        this.tractor = _.uniq(this.tractor);
      }
      else if(res=='crop'){
        this.crop.push(id);
        this.crop = _.uniq(this.crop);
      }
    }
    else{
      // Remove
      if(res=='field'){
        this.field = _.pull(this.field, id);
      }
      else if(res=='tractor'){
        this.tractor = _.pull(this.field, id);
      }
      else if(res=='crop'){
        this.crop = _.pull(this.field, id);
      }
    }
  }

  create(){
    let ts = !_.isEmpty(this.timestamp) ? _.round(new Date(this.timestamp).getTime()/1000) : new Date(("1/1"+"/"+new Date().getFullYear())).getTime() / 1000;
    this.report_err = false;
    console.log('Timestamp ranges',[ts, _.round(new Date().getTime()/1000)]);
    console.log('Fields',this.field);
    console.log('Tractors',this.tractor);
    console.log('Crops',this.crop);

    this.APIService.ListProcessedFields({
      timestamp:{
        between:[ts, _.round(new Date().getTime()/1000)]
      }
    })
    .then((data)=>{
      console.log(data);
      let r = _.filter(data.items,(i)=>{
        if(_.includes([Status.pending_approval, Status.rejected], i.status)){
          return false;
        }
        if(!_.isEmpty(this.field)){
          console.log(this.field, i.field.name, _.includes(this.field,i.field.name));
          return _.includes(this.field,i.field.name);
        }
        else if(!_.isEmpty(this.tractor)){
          return _.includes(this.tractor,i.tractor.name);
        }
        else if(!_.isEmpty(this.crop)){
          return _.includes(this.crop,i.field.crop);
        }
        else{
          return true;
        }
      });
      this.report_err = _.isEmpty(r) ? "Your query did not return any results." : false;
      _.set(this.constant,'report',r);
      this.report = r;
      console.log(r);
      this.area_processed = _.sumBy(r,'area');
      this.fields_processed = _.size(r);
      this.field = [];
      this.tractor = [];
      this.crop = [];
      this.timestamp = null;
      document.querySelector('.create-report .ion-page').scrollTop = 0;
    });
  }

}
