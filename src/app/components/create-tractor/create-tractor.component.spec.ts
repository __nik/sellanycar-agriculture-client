import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateTractorComponent } from './create-tractor.component';
import { FormsModule } from '@angular/forms';
import { APIService } from 'src/app/API.service';
import Amplify, { API } from 'aws-amplify';
import aws_exports from '../../../aws-exports';
import { By } from '@angular/platform-browser';

describe('CreateTractorComponent', () => {
  let component: CreateTractorComponent;
  let fixture: ComponentFixture<CreateTractorComponent>;
  let Api;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateTractorComponent],
      imports: [
        FormsModule,
        IonicModule.forRoot()
      ],
      providers:[
        APIService
      ]
    }).compileComponents();

    Amplify.configure(aws_exports);
    API.configure(aws_exports);

    fixture = TestBed.createComponent(CreateTractorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    Api = fixture.debugElement.injector.get(APIService);
  
    spyOn(Api,'CreateTractor').and.callFake(()=>{ Api.ListTractors() });
    spyOn(Api,'UpdateTractor').and.callFake(()=>{ Api.ListTractors() });
    spyOn(Api,'ListTractors').and.callFake(()=>{return;});

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call Amplify API to make a tractor + list all tractors', ()=>{
    component.ngOnInit();
    component.name = 'Strawberry tractor';
    component.groupsCanAccess = ['users'];
    fixture.detectChanges();
    fixture.debugElement.query(By.css('#create-tractor-button')).triggerEventHandler('click',null);

    expect(Api.CreateTractor).toHaveBeenCalled();
    expect(Api.ListTractors).toHaveBeenCalled();
    expect(Api.CreateTractor).toHaveBeenCalledBefore(Api.ListTractors);
  });

  it('should update if id is present', ()=>{
    component.ngOnInit();

    component.id = 1234;
    component.name = 'Strawberry tractor';
    fixture.detectChanges();
    
    fixture.debugElement.query(By.css('#create-tractor-button')).triggerEventHandler('click',null);

    expect(Api.UpdateTractor).toHaveBeenCalled();
    expect(Api.ListTractors).toHaveBeenCalled();
  })

});
