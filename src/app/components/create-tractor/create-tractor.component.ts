import { Component, OnInit } from '@angular/core';
import { APIService } from 'src/app/API.service';
import { ModalController } from '@ionic/angular';
import { ConstantService } from 'src/app/service/constant.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-create-tractor',
  templateUrl: './create-tractor.component.html',
  styleUrls: ['./create-tractor.component.scss'],
})
export class CreateTractorComponent implements OnInit {

  name;
  id = null;
  groupsCanAccess;
  err = null;

  constructor(
    public APIService: APIService,
    public modalController: ModalController,
    public constant: ConstantService
  ) { }

  ngOnInit() {}

  create(){
    if(_.isEmpty(this.name)){
      this.err = 'Enter a name for the tractor';
      return;
    }
    
    if(this.id != null){
      // update
      this.APIService.UpdateTractor({
        id: this.id,
        name: this.name
      })
      .then(()=>{
        this.APIService.ListTractors()
          .then((tractors) => {
            _.set(this.constant,'tractors',_.orderBy(tractors.items,['name'],['asc']));
            this.modalController.dismiss('update');
          })
          .catch((err) => {
            console.log('Err', err);
          })
      })
    }
    else{
      // create
      this.APIService.CreateTractor({
        name: this.name,
        groupsCanAccess: ['users']
      })
      .then(()=>{
        this.APIService.ListTractors()
          .then((tractors) => {
            _.set(this.constant,'tractors',_.orderBy(tractors.items,['name'],['asc']));
            this.modalController.dismiss('create');
          })
          .catch((err) => {
            console.log('Err', err);
          })
      });
    }
  }

}
