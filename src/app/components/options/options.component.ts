import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss'],
})
export class OptionsComponent implements OnInit {

  opts;
  resource;
  id;

  constructor(
    public popoverController: PopoverController
  ) { }

  ngOnInit() { }

}
