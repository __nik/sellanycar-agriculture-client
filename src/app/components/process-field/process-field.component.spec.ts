import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProcessFieldComponent } from './process-field.component';
import { FormsModule } from '@angular/forms';
import { APIService } from 'src/app/API.service';
import Amplify, { API } from 'aws-amplify';
import aws_exports from '../../../aws-exports';

describe('Process Field component', () => {
  let component: ProcessFieldComponent;
  let fixture: ComponentFixture<ProcessFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessFieldComponent ],
      imports: [
        IonicModule.forRoot(),
        FormsModule
      ],
      providers: [
        APIService
      ]
    }).compileComponents();

    Amplify.configure(aws_exports);
    API.configure(aws_exports);

    fixture = TestBed.createComponent(ProcessFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
