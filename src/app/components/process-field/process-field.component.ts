import { Component, OnInit } from '@angular/core';
import { ConstantService } from 'src/app/service/constant.service';
import { ModalController } from '@ionic/angular';
import { APIService, Status } from 'src/app/API.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-process-field',
  templateUrl: './process-field.component.html',
  styleUrls: ['./process-field.component.scss'],
})
export class ProcessFieldComponent implements OnInit {

  id = null;
  groupsCanAccess;
  err = null;
  timestamp;
  datestring;
  area;
  max_area = 1;
  tractor;
  field;
  fields;
  tractors;

  constructor(
    public APIService: APIService,
    public modalController: ModalController,
    public constant: ConstantService
  ) { }

  ngOnInit() {
    console.log(this.tractors);
  }

  change(res,ev){
    if(res == 'field'){
      let g = _.find(this.fields, (f)=>{ return f.id == ev.target.value });
      this.max_area = parseFloat(g.area);
    }
  }

  create(){
    if(_.isEmpty(this.tractor)){
      this.err = 'Select a tractor';
      return;
    }
    if(_.isEmpty(this.field)){
      this.err = 'Select a field';
      return;
    }
    if(_.isEmpty(this.timestamp)){
      this.err = 'Select a time to process the field';
      return;
    }
    if(this.area < 1){
      this.err = 'How much of the field do you want to process?';
      return;
    }
    if(this.area > this.max_area){
      this.err = "You can't process more than the area of the field.";
      return;
    }
    
    if(this.id != null){
      console.log('Update process');return;
      // update
      this.APIService.UpdateField({
        id: this.id,
        area: this.area,
      })
      .then(()=>{
        this.APIService.ListFields()
          .then((fields) => {
            _.set(this.constant,'fields',_.orderBy(fields.items,['name'],['asc']));
            this.modalController.dismiss('update');
          })
          .catch((err) => {
            console.log('Err', err);
          })
      })
    }
    else{
      // create
      let time = new Date(this.timestamp);
      this.APIService.CreateProcessedField({
        timestamp: _.round(time.getTime()/1000),
        datestring: time.getDate() + "/" + (Number(time.getMonth())+1) + "/" + time.getFullYear(),
        area: this.area,
        processedFieldTractorId: this.tractor,
        processedFieldFieldId: this.field,
        status: Status.pending_approval, 
        groupsCanAccess: ['users']
      })
      .then(()=>{
        this.APIService.ListProcessedFields()
          .then((processes) => {
            _.set(this.constant,'processes',_.orderBy(processes.items,['timestamp'],['desc']));
            this.modalController.dismiss('create');
          })
          .catch((err) => {
            console.log('Err', err);
          })
      });
    }
  }

}
