import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-tab-actions',
  templateUrl: './tab-actions.component.html',
  styleUrls: ['./tab-actions.component.scss'],
})
export class TabActionsComponent implements OnInit {

  page;
  constructor(
    public popoverController: PopoverController
  ) { }

  ngOnInit() {}

}
