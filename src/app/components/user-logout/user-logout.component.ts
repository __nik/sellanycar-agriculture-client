import { Component, OnInit } from '@angular/core';
import { Auth } from 'aws-amplify';
import { Router } from '@angular/router';
import { NavController, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-user-logout',
  templateUrl: './user-logout.component.html',
  styleUrls: ['./user-logout.component.scss'],
})
export class UserLogoutComponent implements OnInit {

  user;
  constructor(
    public router: Router,
    public navCtrl: NavController,
    public popoverController: PopoverController
  ) { }

  ngOnInit() {}

  logout() {
    Auth.signOut()
      .then(data => {
        console.log('Logout', data);
        this.navCtrl.setDirection('root');
        this.router.navigate(['/']);
      })
      .catch(err => {
        console.log('Logout error', err);
      });
  }

}
