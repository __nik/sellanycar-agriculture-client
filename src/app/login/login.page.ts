import { Component, AfterContentInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthGuardService } from '../service/auth-route-guard.service'
import { AmplifyService }  from 'aws-amplify-angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements AfterContentInit {

  authState: any;
  authService: AuthGuardService;
  amplifyService: AmplifyService;
  amplifySignUpConfig = {
    header: "Sign up for an account",
    hideAllDefaults: true,
    signUpFields: [
      {
        "label": "Username",
        "key": "username",
        "required": true,
        "displayOrder": 1,
        "type": "string"
      },
      {
        "label": "Password",
        "key": "password",
        "required": true,
        "displayOrder": 2,
        "type": "password"
      },
      {
        "label": "Email",
        "key": "email",
        "required": true,
        "displayOrder": 3,
        "type": "string"
      }
    ]
  }

  constructor(
    public guard: AuthGuardService,
    public amplify: AmplifyService,
    private router: Router,
    private navCtrl: NavController,
  ) {
    this.authState = {loggedIn: false};
    this.authService = guard;
    this.amplifyService = amplify;
    this.amplifyService.authStateChange$
    .subscribe(authState => {
      this.authState.loggedIn = authState.state === 'signedIn';
      if(authState.state == 'signedIn'){
        this.navCtrl.setDirection('root');
        this.router.navigate(['/app/tabs/fields']);
      }
    });
  }

  ionViewWillEnter() {
    
  }

  ngAfterContentInit(){
    // this.events.publish('data:AuthState', this.authState)
  }

}
