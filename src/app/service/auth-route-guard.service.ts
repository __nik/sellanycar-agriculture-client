import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AmplifyService } from 'aws-amplify-angular';
import * as _ from 'lodash';
import { ConstantService } from './constant.service';

@Injectable()
export class AuthGuardService implements CanActivate {

  signedIn: boolean = false;
  amplifyService: AmplifyService;

  constructor(
    public router: Router,
    public constant: ConstantService,
    public amplify: AmplifyService,
  ) { 
    this.amplifyService = amplify;
    this.amplifyService.authStateChange$
      .subscribe(async (authState) => {
        console.log(authState,'service');
        if (_.get(authState, 'state', 'signedOut') == 'signedIn') {
          this.signedIn = true;
          // User
          let user = await this.amplifyService.auth().currentUserInfo();
          _.set(this.constant, 'user', user);
        } else {
          this.signedIn = false;
          console.log('User is not logged in');
        }
      });
  }

  canActivate() {
    if (!this.signedIn) {
      this.router.navigate(['/login']);
    }
    return this.signedIn;
  }
}