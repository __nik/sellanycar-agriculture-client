import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement, NO_ERRORS_SCHEMA } from "@angular/core";
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';
import Amplify, { API } from 'aws-amplify';
import aws_exports from '../../aws-exports';
import { By } from "@angular/platform-browser";

import { Tab1Page } from './tab1.page';
import { RouterTestingModule } from '@angular/router/testing';
import { APIService } from '../API.service';
import { CreateFieldComponent } from '../components/create-field/create-field.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OptionsComponent } from '../components/options/options.component';

describe('Fields Page', () => {
  let component: Tab1Page;
  let fixture: ComponentFixture<Tab1Page>;
  let Api;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Tab1Page, CreateFieldComponent, OptionsComponent],
      imports: [
        IonicModule.forRoot(),
        ExploreContainerComponentModule,
        RouterTestingModule,
        FormsModule
      ],
      providers: [
        APIService
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
    Amplify.configure(aws_exports);
    API.configure(aws_exports);

    fixture = TestBed.createComponent(Tab1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();

    Api = fixture.debugElement.injector.get(APIService);

    spyOn(Api, 'ListFields').and.callFake(()=>{return;});
    spyOn(Api, 'DeleteField').and.callFake(()=>{return new Promise((res,rej)=>{res()});});
    spyOn(component, 'createFieldModal');
    spyOn(component, 'optionsPopover');

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should query Amplify API for records', () => {
    component.ngOnInit();
    expect(Api.ListFields).toHaveBeenCalled();
  });

  it('should show empty prompt when records are empty', () => {
    component.ngOnInit();
    component.fields = [];
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('#empty-prompt'))).not.toBeNull();
    expect(fixture.debugElement.query(By.css('#fields-holder'))).toBeNull();
  });

  it('should show fields when records are available', () => {
    component.ngOnInit();
    component.fields = [
      {
        id: "some-aws-generated-id",
        name: "Strawberry field",
        crop: "Strawberry",
        area: 148.9
      }
    ];
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('#empty-prompt'))).toBeNull();
    expect(fixture.debugElement.query(By.css('#fields-holder'))).not.toBeNull();
  });

  it('should show options popover only if owner', () => {
    component.ngOnInit();
    component.fields = [
      {
        id: "some-aws-generated-id",
        name: "Strawberry field",
        crop: "Strawberry",
        area: 148.9
      }
    ];
    fixture.detectChanges();

    fixture.debugElement.query(By.css('#fields-holder ion-icon')).triggerEventHandler('click', null);
    expect(component.optionsPopover).toHaveBeenCalled();
  });

  it('should delete field', () => {
    component.ngOnInit();
    component.delete('123');

    expect(Api.DeleteField).toHaveBeenCalled();
  });

});
