import { Component, OnInit } from '@angular/core';
import { Auth } from 'aws-amplify';
import { NavController, ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { APIService } from '../API.service';
import * as _ from 'lodash';
import { CreateFieldComponent } from '../components/create-field/create-field.component';
import { ConstantService } from '../service/constant.service';
import { OptionsComponent } from '../components/options/options.component';
import { UserLogoutComponent } from '../components/user-logout/user-logout.component';
import { TabActionsComponent } from '../components/tab-actions/tab-actions.component';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  fields = [];
  loading = true;
  username;

  constructor(
    public router: Router,
    public navCtrl: NavController,
    public APIService: APIService,
    public modalController: ModalController,
    public popoverController: PopoverController,
    public constant: ConstantService,
    public toastController: ToastController
  ) {}

  async ngOnInit() {   
    this.APIService.ListFields()
      .then((data) => {
        this.fields = data.items;
        _.set(this.constant,'fields',data.items);
      })
      .catch((err) => {
        console.log('Err', err);
      })
      .finally(()=>{
        this.loading = false;
        this.username = _.get(this.constant,'user.username');
      });
  }

  ionViewDidEnter(){
    if(!_.isEmpty(_.get(this.constant,'fields'))){
      this.fields = _.get(this.constant,'fields');
    }
  }

  async createFieldModal(componentProps = null){
    const modal = await this.modalController.create({
      component: CreateFieldComponent,
      componentProps: componentProps
    });
    modal.onDidDismiss()
    .then((data)=>{ console.log('Modal dismiss',data);
      if(!_.isEmpty(_.get(this.constant,'fields'))){
        this.fields = _.get(this.constant,'fields');
      }
      if(data.data == 'update'){
        this.presentToast('Field has been updated');
      }
      else if(data.data == 'delete'){
        this.presentToast('Field has been deleted');
      }
    });
    return await modal.present();
  }

  async optionsPopover(ev: any, field) {
    const popover = await this.popoverController.create({
      component: OptionsComponent,
      event: ev,
      componentProps:{
        opts: ((_.get(this.constant,'user.username') == field.owner) || (_.get(this.constant,'user.username') == 'admin')) ? true : false,
        resource: 'field',
        id: field.id
      },
      translucent: true
    });
    popover.onDidDismiss()
    .then((data)=>{
      if(_.get(data.data,'action') == "update"){
        // field
        let field = _.find(_.get(this.constant,'fields'), {id:_.get(data.data,'id')});        
        this.createFieldModal({name:field.name,id:field.id,crop:field.crop,area:field.area});
      }
      else if(_.get(data.data,'action') == 'delete'){
        this.delete(_.get(data.data,'id'));
      }
    });
    return await popover.present();
  }

  async userPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: UserLogoutComponent,
      event: ev,
      componentProps:{
        page: 'field',
        user: _.get(this.constant,'user.username')
      },
      translucent: true
    });
    return await popover.present();
  }

  async tabActionsPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: TabActionsComponent,
      event: ev,
      componentProps:{
        page: 'FIELD'
      },
      translucent: true
    });
    popover.onDidDismiss()
    .then((data)=>{
      if(data.data == 'modal'){
        this.createFieldModal();
      }
    })
    return await popover.present();
  }
  
  delete(id){
    this.APIService.DeleteField({id:id})
    .then(()=>{
      this.APIService.ListFields()
          .then((fields) => {
            _.set(this.constant,'fields',_.orderBy(fields.items,['name'],['asc']));
            this.fields = _.get(this.constant,'fields');
            this.presentToast('Field has been deleted');
          })
          .catch((err) => {
            console.log('Err', err);
          })
    })
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      color: 'success',
      duration: 5000
    });
    return await toast.present();
  }
  
}
