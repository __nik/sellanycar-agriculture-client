import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';
import Amplify, { API } from 'aws-amplify';
import aws_exports from '../../aws-exports';
import { By } from "@angular/platform-browser";

import { Tab2Page } from './tab2.page';
import { RouterTestingModule } from '@angular/router/testing';
import { APIService } from '../API.service';
import { FormsModule } from '@angular/forms';
import { OptionsComponent } from '../components/options/options.component';
import { CreateTractorComponent } from '../components/create-tractor/create-tractor.component';

describe('Tractors Page', () => {
  let component: Tab2Page;
  let fixture: ComponentFixture<Tab2Page>;
  let Api;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Tab2Page, CreateTractorComponent, OptionsComponent],
      imports: [
        IonicModule.forRoot(),
        ExploreContainerComponentModule,
        RouterTestingModule,
        FormsModule
      ],
      providers: [
        APIService
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
    Amplify.configure(aws_exports);
    API.configure(aws_exports);

    fixture = TestBed.createComponent(Tab2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();

    Api = fixture.debugElement.injector.get(APIService);

    spyOn(Api, 'ListTractors').and.callFake(()=>{return;});
    spyOn(Api, 'DeleteTractor').and.callFake(()=>{return new Promise((res,rej)=>{res()});});
    spyOn(component, 'createTractorModal');
    spyOn(component, 'optionsPopover');

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should query Amplify API for records', () => {
    component.ngOnInit();
    expect(Api.ListTractors).toHaveBeenCalled();
  });

  it('should show empty prompt when records are empty', () => {
    component.ngOnInit();
    component.tractors = [];
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('#empty-prompt'))).not.toBeNull();
    expect(fixture.debugElement.query(By.css('#tractors-holder'))).toBeNull();
  });

  it('should show tractors when records are available', () => {
    component.ngOnInit();
    component.tractors = [
      {
        id: "some-aws-generated-id",
        name: "Strawberry tractor"
      }
    ];
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('#empty-prompt'))).toBeNull();
    expect(fixture.debugElement.query(By.css('#tractors-holder'))).not.toBeNull();
  });

  it('should show create modal', () => {
    component.ngOnInit();
    component.createTractorModal();
    expect(component.createTractorModal).toHaveBeenCalled();
  });

  it('should show options popover only if owner', () => {
    component.ngOnInit();
    component.tractors = [
      {
        id: "some-aws-generated-id",
        name: "Strawberry tractor"
      }
    ];
    fixture.detectChanges();

    fixture.debugElement.query(By.css('#tractors-holder ion-icon')).triggerEventHandler('click', null);
    expect(component.optionsPopover).toHaveBeenCalled();
  });

  it('should delete tractor', () => {
    component.ngOnInit();
    component.delete('123');

    expect(Api.DeleteTractor).toHaveBeenCalled();
  });

});
