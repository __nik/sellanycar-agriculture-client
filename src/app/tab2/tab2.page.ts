import { Component, OnInit } from '@angular/core';
import { Auth } from 'aws-amplify';
import { NavController, ModalController, PopoverController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { APIService } from '../API.service';
import * as _ from 'lodash';
import { ConstantService } from '../service/constant.service';
import { OptionsComponent } from '../components/options/options.component';
import { CreateTractorComponent } from '../components/create-tractor/create-tractor.component';
import { UserLogoutComponent } from '../components/user-logout/user-logout.component';
import { TabActionsComponent } from '../components/tab-actions/tab-actions.component';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  tractors = [];
  loading = true;
  username;

  constructor(
    public router: Router,
    public navCtrl: NavController,
    public APIService: APIService,
    public modalController: ModalController,
    public popoverController: PopoverController,
    public constant: ConstantService,
    public toastController: ToastController
  ) {}

  async ngOnInit() {   
    this.APIService.ListTractors()
      .then((data) => {
        this.tractors = data.items;
        _.set(this.constant,'tractors',data.items);
      })
      .catch((err) => {
        console.log('Err', err);
      })
      .finally(()=>{
        this.username = _.get(this.constant,'user.username');
      });
  }

  ionViewDidEnter(){
    if(!_.isEmpty(_.get(this.constant,'tractors'))){
      this.tractors = _.get(this.constant,'tractors');
    }
  }

  async createTractorModal(componentProps = null){
    const modal = await this.modalController.create({
      component: CreateTractorComponent,
      componentProps: componentProps,
      cssClass: 'tractor'
    });
    modal.onDidDismiss()
    .then((data)=>{ console.log('Modal dismiss',data);
      if(!_.isEmpty(_.get(this.constant,'tractors'))){
        this.tractors = _.get(this.constant,'tractors');
      }
      if(data.data == 'update'){
        this.presentToast('Tractor has been updated');
      }
      else if(data.data == 'delete'){
        this.presentToast('Tractor has been deleted');
      }
    });
    return await modal.present();
  }

  async optionsPopover(ev: any, tractor) {
    const popover = await this.popoverController.create({
      component: OptionsComponent,
      event: ev,
      componentProps:{
        opts: ((_.get(this.constant,'user.username') == tractor.owner) || (_.get(this.constant,'user.username') == 'admin')) ? true : false,
        resource: 'tractor',
        id: tractor.id
      },
      translucent: true
    });
    popover.onDidDismiss()
    .then((data)=>{
      if(_.get(data.data,'action') == "update"){
        // tractor
        let tractor = _.find(_.get(this.constant,'tractors'), {id:_.get(data.data,'id')});        
        this.createTractorModal({name:tractor.name,id:tractor.id});
      }
      else if(_.get(data.data,'action') == 'delete'){
        this.delete(_.get(data.data,'id'));
      }
    });
    return await popover.present();
  }

  async userPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: UserLogoutComponent,
      event: ev,
      componentProps:{
        page: 'tractor',
        user: _.get(this.constant,'user.username')
      },
      translucent: true
    });
    return await popover.present();
  }

  async tabActionsPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: TabActionsComponent,
      event: ev,
      componentProps:{
        page: 'TRACTOR'
      },
      translucent: true
    });
    popover.onDidDismiss()
    .then((data)=>{
      if(data.data == 'modal'){
        this.createTractorModal();
      }
    })
    return await popover.present();
  }

  delete(id){
    this.APIService.DeleteTractor({id:id})
    .then(()=>{
      this.APIService.ListTractors()
      .then((data) => {
        this.tractors = data.items;
        _.set(this.constant,'tractors',data.items);
      })
      .catch((err) => {
        console.log('Err', err);
      });
    })
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      color: 'success',
      duration: 5000
    });
    return await toast.present();
  }

  logout() {
    Auth.signOut()
      .then(data => {
        console.log('Logout', data);
        this.navCtrl.setDirection('root');
        this.router.navigate(['/']);
      })
      .catch(err => {
        console.log('Logout error', err);
      });
  }

}
