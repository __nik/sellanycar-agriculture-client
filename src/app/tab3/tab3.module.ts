import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab3Page } from './tab3.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';
import { ProcessFieldComponent } from '../components/process-field/process-field.component';
import { ApproveComponent } from '../components/approve/approve.component';
import { CreateReportModalComponent } from '../components/create-report-modal/create-report-modal.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    RouterModule.forChild([{ path: '', component: Tab3Page }])
  ],
  declarations: [Tab3Page, ProcessFieldComponent, ApproveComponent, CreateReportModalComponent],
  entryComponents:[ProcessFieldComponent, ApproveComponent, CreateReportModalComponent]
})
export class Tab3PageModule {}
