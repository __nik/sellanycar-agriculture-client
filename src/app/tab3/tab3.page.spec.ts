import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';
import Amplify, { API } from 'aws-amplify';
import aws_exports from '../../aws-exports';
import { By } from "@angular/platform-browser";

import { Tab3Page } from './tab3.page';
import { RouterTestingModule } from '@angular/router/testing';
import { APIService } from '../API.service';
import { FormsModule } from '@angular/forms';
import { ProcessFieldComponent } from '../components/process-field/process-field.component';
import { ApproveComponent } from '../components/approve/approve.component';

describe('Process Fields page', () => {
  let component: Tab3Page;
  let fixture: ComponentFixture<Tab3Page>;
  let Api;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Tab3Page, ProcessFieldComponent, ApproveComponent],
      imports: [
        IonicModule.forRoot(),
        ExploreContainerComponentModule,
        RouterTestingModule,
        FormsModule
      ],
      providers: [
        APIService
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
    
    Amplify.configure(aws_exports);
    API.configure(aws_exports);

    fixture = TestBed.createComponent(Tab3Page);
    component = fixture.componentInstance;
    
    fixture.detectChanges();

    Api = fixture.debugElement.injector.get(APIService);

    spyOn(Api, 'ListProcessedFields').and.callFake(()=>{return;});
    spyOn(Api, 'UpdateProcessedField').and.callFake(()=>{return new Promise((res,rej)=>{res()});});
    spyOn(Api, 'ListFields').and.callFake(()=>{return new Promise((res,rej)=>{res()});});
    spyOn(Api, 'ListTractors').and.callFake(()=>{return new Promise((res,rej)=>{res()});});
    spyOn(component, 'optionsPopover');
    spyOn(component, 'createProcessModal');

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should query Amplify API for records', () => {
    component.ngOnInit();
    expect(Api.ListProcessedFields).toHaveBeenCalled();
  });

  it('should show empty prompt when records are empty', () => {
    component.ngOnInit();
    component.processes = [];
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('#empty-prompt'))).not.toBeNull();
    expect(fixture.debugElement.query(By.css('#processes-holder'))).toBeNull();
  });

  it('should show tractors when records are available', () => {
    component.ngOnInit();
    component.processes = [
      {
        id: "some-aws-generated-id",
        timestamp: 123455678,
        datestring: "12/5/2020",
        area: 114,
        tractor:{
          name: "Some tractor"
        },
        field:{
          name: "Some field"
        },
        status: "pending_approval",
        groupsCanAccess: ['users']
      }
    ];
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('#empty-prompt'))).toBeNull();
    expect(fixture.debugElement.query(By.css('#processes-holder'))).not.toBeNull();
  });

  it('should show create modal', () => {
    component.ngOnInit();
    component.createProcessModal();
    expect(component.createProcessModal).toHaveBeenCalled();
  });

  it('should show options popover only if admin', () => {
    component.ngOnInit();
    component.processes = [
      {
        id: "some-aws-generated-id",
        timestamp: 123455678,
        datestring: "12/5/2020",
        area: 114,
        tractor:{
          name: "Some tractor"
        },
        field:{
          name: "Some field"
        },
        status: "pending_approval",
        groupsCanAccess: ['users']
      }
    ];
    component.username = 'admin';
    fixture.detectChanges();

    fixture.debugElement.query(By.css('#processes-holder ion-icon')).triggerEventHandler('click', null);
    expect(component.optionsPopover).toHaveBeenCalled();
  });

});
