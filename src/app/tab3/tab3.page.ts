import { Component, OnInit } from '@angular/core';
import { Auth } from 'aws-amplify';
import { NavController, ModalController, ToastController, PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';
import { APIService, Status } from '../API.service';
import * as _ from 'lodash';
import { ConstantService } from '../service/constant.service';
import { ProcessFieldComponent } from '../components/process-field/process-field.component';
import { ApproveComponent } from '../components/approve/approve.component';
import { CreateReportModalComponent } from '../components/create-report-modal/create-report-modal.component';
import { UserLogoutComponent } from '../components/user-logout/user-logout.component';
import { TabActionsComponent } from '../components/tab-actions/tab-actions.component';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {

  processes = [];
  loading = true;
  username;

  constructor(
    public router: Router,
    public navCtrl: NavController,
    public APIService: APIService,
    public modalController: ModalController,
    public constant: ConstantService,
    public toastController: ToastController,
    public popoverController: PopoverController
  ) {}

  async ngOnInit() {   
    this.APIService.ListProcessedFields()
      .then((data) => {
        this.processes = data.items;
        _.set(this.constant,'processes',data.items);
      })
      .catch((err) => {
        console.log('Err', err);
      })
      .finally(()=>{
        this.username = _.get(this.constant,'user.username');
      });

  }

  ionViewDidEnter(){
    if(!_.isEmpty(_.get(this.constant,'processes'))){
      this.processes = _.get(this.constant,'processes');
    }
  }

  async optionsPopover(ev: any, id) {
    const popover = await this.popoverController.create({
      component: ApproveComponent,
      event: ev,
      componentProps:{
        id: id
      },
      translucent: true
    });
    popover.onDidDismiss()
    .then((data)=>{
      if(_.has(data,'data.id')){
        this.update(_.get(data,'data.action'), _.get(data,'data.id'));
      }
    });
    return await popover.present();
  }

  update(action, id){
    let status = (action == "approve") ? Status.ready_to_process : Status.rejected
    this.APIService.UpdateProcessedField({
      id:id,
      status: status
    })
    .then(()=>{
      this.APIService.ListProcessedFields()
      .then((data) => {
        this.processes = data.items;
        _.set(this.constant,'processes',data.items);
      })
      .catch((err) => {
        console.log('Err', err);
      });
    })
  }

  async createReportModal(){
    let fields = [];
    let tractors = [];
    this.APIService.ListFields()
      .then((data) => {
        _.set(this.constant,'fields',data.items);
        fields = data.items;

        this.APIService.ListTractors()
        .then(async (data) => {
          _.set(this.constant,'tractors',data.items);
          tractors = data.items;

          const modal = await this.modalController.create({
            component: CreateReportModalComponent,
            componentProps: {
              fields: fields,
              tractors: tractors
            },
            cssClass: "create-report"
          });
          modal.onDidDismiss()
          .then((data)=>{
            console.log('report dismissed',data);
          });
          return await modal.present();  
        })
        .catch((err) => {
          console.log('Err', err);
        });
      })
      .catch((err) => {
        console.log('Err', err);
      });    
  }

  async createProcessModal(){
    let fields = [];
    let tractors = [];
    this.APIService.ListFields()
      .then((data) => {
        _.set(this.constant,'fields',data.items);
        fields = data.items;

        this.APIService.ListTractors()
        .then(async (data) => {
          _.set(this.constant,'tractors',data.items);
          tractors = data.items;

          const modal = await this.modalController.create({
            component: ProcessFieldComponent,
            componentProps: {
              fields: fields,
              tractors: tractors
            },
            cssClass: "process-field"
          });
          modal.onDidDismiss()
          .then((data)=>{ console.log('Modal dismiss',data);
            if(!_.isEmpty(_.get(this.constant,'processes'))){
              this.processes = _.get(this.constant,'processes');
            }
            if(data.data == 'update'){
              this.presentToast('Field has been updated');
            }
          });
          return await modal.present();
        })
        .catch((err) => {
          console.log('Err', err);
        });
      })
      .catch((err) => {
        console.log('Err', err);
      });    
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      color: 'success',
      duration: 5000
    });
    return await toast.present();
  }

  async userPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: UserLogoutComponent,
      event: ev,
      componentProps:{
        page: 'tractor',
        user: _.get(this.constant,'user.username')
      },
      translucent: true
    });
    return await popover.present();
  }

  async tabActionsPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: TabActionsComponent,
      event: ev,
      componentProps:{
        page: 'REPORT'
      },
      translucent: true
    });
    popover.onDidDismiss()
    .then((data)=>{
      if(data.data == 'modal'){
        this.createProcessModal();
      }
      else if(data.data == 'report'){
        this.createReportModal();
      }
    })
    return await popover.present();
  }

  logout() {
    Auth.signOut()
      .then(data => {
        console.log('Logout', data);
        this.navCtrl.setDirection('root');
        this.router.navigate(['/']);
      })
      .catch(err => {
        console.log('Logout error', err);
      });
  }

}
