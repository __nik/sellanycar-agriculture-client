import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs-routing.module';

import { TabsPage } from './tabs.page';
import { OptionsComponent } from '../components/options/options.component';
import { UserLogoutComponent } from '../components/user-logout/user-logout.component';
import { TabActionsComponent } from '../components/tab-actions/tab-actions.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule
  ],
  declarations: [TabsPage, OptionsComponent, UserLogoutComponent, TabActionsComponent],
  entryComponents: [OptionsComponent, UserLogoutComponent, TabActionsComponent]
})
export class TabsPageModule {}
